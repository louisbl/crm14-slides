class: center, middle, inverse
# Initiation Mobile Java/Android
## CRM14 - 1ère année

---
class: center, middle, inverse
# Bonjour !

---
.left-column[
## Louis Beltramo
### - SRC Montbéliard / Gobelins Annecy
### - Freelance / Bocalènco / Oz
### - Développeur mobile & web
]

.right-column[
.pull-left[
### Un soupçon de desktop
- C
- Java
- AS3/Air
- Haxe/NME/OpenFL
- NW.js

### Un peu de back
- PHP/Wordpress
- Ruby/Rails
- Node.js/Hapi

### Du mobile
- AS3/Air
- Haxe/NME/OpenFL
- Java/Android
- JS/Cordova
]

### Quelques plateformes
- Android
- Arduino
- Blackberry
- Firefox OS
- GNU/Linux
- iOS
- Matchstick
- Raspberry Pi
- Tizen (IVI)
- Web
- Windows
]

---
class: center, middle, inverse
# Modalités

---
.left-column[
## Modalités
### - Rendu
]

.right-column[
# .center[Aucun]
]

---
.left-column[
## Modalités
### - Rendu
### - Planning
]

.right-column[
# Lundi 19 janvier
## matin
- Un peu de théorie
- Prise en main d'Android Studio
- Devices et émulateurs
- Hello World

## après-midi
- Architecture et composants Android
  - Manifest
  - Activity et explicit Intents
  - Layouts
  - Fragments
  - Ressources
  - Adapter et ListView
  - Shared Preferences
]

---
.left-column[
## Modalités
### - Rendu
### - Planning
]

.right-column[
# Mardi 20 janvier
## matin
- JSON REST API FTW
- Threads
]

---
.left-column[
## Modalités
### - Rendu
### - Planning
]

.right-column[
# Vendredi 23 janvier
## après-midi
- Cycle de vie des Activities
- Implicit Intent
- Easy Share
]

---
.left-column[
## Modalités
### - Rendu
### - Planning
]

.right-column[
# Lundi 2 février
## matin
- Content provider

## après-midi
- Sensors
]

---
.left-column[
## Modalités
### - Rendu
### - Planning
]

.right-column[
# Mardi 3 février
## matin
- Medias
- ...
]

---
class: center, middle, inverse
# Android

---
.left-column[
## Android
### - Android, Inc
]

.right-column[
## Fondé en Octobre 2003
- concurrencer Symbian (!) et Windows Mobile (!)
- le but : "smarter mobile devices that are more aware of its owner's location and preferences"
<br />
<br />
.pull-left[![img](./images/juggle.gif)]
]

.footnote[Pour info en 2002 on vendait encore des Nokia 3310]

---
.left-column[
## Android
### - Android, Inc
### - Google
]

.right-column[
## Racheté par Google en Août 2005
- Travail sur une platforme mobile basée sur le noyau Linux
- Protoype proche d'un blackberry
<br/>
<br/>
.pull-left[![tiny_img](https://upload.wikimedia.org/wikipedia/commons/5/51/Android_mobile_phone_platform_early_device.jpg)]
]

---
.left-column[
## Android
### - Android, Inc
### - Google
### - Touchscreen
]

.right-column[
## iPhone sort en 2007
- Pas de clavier physique !
- Google se dépêche d'ajouter un touchscreen mais garde le clavier physique.

## HTC Dream (G1) sort en 2008
<br/>
<br/>
.center[![tiny_img](https://upload.wikimedia.org/wikipedia/commons/b/be/HTC_Dream_Orange_FR.jpeg)]
]

---
.left-column[
## Android
### - Android, Inc
### - Google
### - Touchscreen
### - Nexus
]

.right-column[
## Nexus One en 2010
- Android Eclair 2.1
<br/>
<br/>
.pull-left[![tiny_img](https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Nexus_One.png/300px-Nexus_One.png)]
]

---
.left-column[
## Android
### - Android, Inc
### - Google
### - Touchscreen
### - Nexus
### - Versions
]

.right-column[
## D'android Alpha à Android Lollipop
.center[![tiny_img](https://upload.wikimedia.org/wikipedia/commons/e/ee/Android_historical_version_distribution_-_vector.svg)]
https://en.wikipedia.org/wiki/Android_version_history
]

---
.left-column[
## Android
### - Android, Inc
### - Google
### - Touchscreen
### - Nexus
### - Versions
### - AOSP
]

.right-column[
# Android Open Source Project
- Couche propriétaire Google
- De nombreuses applis AOSP sont abandonnées

## Plusieurs Android
- Cyanogen
- Replicant

## Des morceaux d'Android ailleurs
- Firefox OS
- Jolla Sailfish OS
]

---
class: center, middle, inverse
# Android 5


---
.left-column[
## Android 5
]

.center[
![img](./images/juggle_lollipop.gif)
]

---
.left-column[
## Android 5
### - Material Design
]

.center[
<video autoplay="true" loop="true">
    <source src="https://developer.android.com/design/material/videos/ContactsAnim.mp4" />
</video>
]

.footnote[[->Intro material design](http://www.google.com/design/spec/material-design/introduction.html)]

---
.left-column[
## Android 5
### - Material Design
### - Nouveautés
]
.right-column[
.pull-right[![tiny_img](https://developer.android.com/images/android-5.0/notifications/hun-example.png)]
## Notifications
## Project Volta
## ART
- Remplace Dalvik
- AOT au lieu de JIT

]

???
__ART__ Ahead-of-time (AOT) compilation

---
.left-column[
## Android 5
### - Material Design
### - Nouveautés
### - Android Wear
]

.right-column[
.pull-right[![Right-aligned image](https://developer.android.com/wear/images/notifications.png)]
## Mobile & Wear
- Applis "déportés"
]

---
.left-column[
## Android 5
### - Material Design
### - Nouveautés
### - Android Wear
### - Android TV
]

.pull-left[![full_img](https://developer.android.com/tv/images/atv-framed.png)]

---
.left-column[
## Android 5
### - Material Design
### - Nouveautés
### - Android Wear
### - Android TV
### - Android Auto
]

.pull-left[![full_img](http://www.android.com/new/images/auto/slide-1.jpg)]

---
.left-column[
## Android 5
### - Material Design
### - Nouveautés
### - Android Wear
### - Android TV
### - Android Auto
### - OK Google
]

.pull-left[![full_img](http://www.android.com/new/images/auto/slide-4.jpg)]

---
class: center, middle, inverse
# Android Studio

---
.left-column[
## Android Studio
### - ADT ?
]

.right-column[
## ADT
- Eclipse
- Ant
- Abandonné par Google

## Android Studio
- IntelliJ Idea
- Gradle
- IDE officiel pour Android
]
.right[![Right-aligned image](https://1.bp.blogspot.com/-u5dfSsMOMC0/UZO_5DC_W9I/AAAAAAAACM8/YCMn15HPzpE/s1600/Studio_table.png)]

---
.left-column[
## Android Studio
### - ADT ?
### - Gradle
]

.right-column[
.pull-right[![Right-aligned image](images/android_build_process.png)]
## Automate everything
- Gestion des assets
- Gestion des librairies
- Compilation
- Build de l'APK
- Signature
- ...

## Groovy
- Langage dynamique pour la JVM

```groovy
def commitHg(message) {
    def stdout = new ByteArrayOutputStream()
    exec {
        commandLine "hg", "commit", "-A", "-m", message
        standardOutput = stdout
    }
    println stdout.toString()
}

```

.footnote[Ex: Grunt, Gulp, Robo, Rake, Leinigen...]
]

???


---
.left-column[
## Android Studio
### - ADT ?
### - Gradle
### - Maven
]

.right-column[
## Dépôt centralisé

```groovy
import java.util.regex.Pattern

apply plugin: 'com.android.application'

repositories {
    mavenCentral()
}
```

## Gestionnaire de dépendances
```groovy
    compile 'com.doomonafireball.betterpickers:library:1.5.+'
    compile 'com.android.support:support-v4:20.0.+'
    compile 'com.google.android.gms:play-services:4.4.+'
```
]

???

- Gestionnaire de dépendance (entre autres)
- PHP : Composer
- JS : NPM / bower
- Ruby : Gem / bundler

---
class: center, middle, inverse
# Git

---
.left-column[
## Git
### - Github.com
### - Bitbucket.org
### - Gitlab.com
]

.right-column[
## Pour visualiser tout ça
- https://windows.github.com/
- https://mac.github.com/
- http://www.sourcetreeapp.com/
- https://github.com/FredrikNoren/ungit
]

---
